import * as React from "react";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import DashboardIcon from "@mui/icons-material/Dashboard";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PeopleIcon from "@mui/icons-material/People";
import BarChartIcon from "@mui/icons-material/BarChart";
import LayersIcon from "@mui/icons-material/Layers";
import AssignmentIcon from "@mui/icons-material/Assignment";
import CategoryIcon from "@mui/icons-material/Category";
import InventoryIcon from "@mui/icons-material/Inventory";
import ImportExportIcon from "@mui/icons-material/ImportExport";
export const listItems = [
  {
    id: 1,
    name: "Trang chủ",
    icon: <DashboardIcon />,
    path: "/admin/dashboard",
  },
  {
    id: 2,
    name: "Quản lý danh mục",
    icon: <CategoryIcon />,
    children: [
      {
        id: 3,
        name: "Danh sách danh mục",
        path: "/admin/category",
      },
      {
        id: 4,
        name: "Thuộc tính",
        path: "/admin/category/attribute",
      },
      {
        id: 5,
        name: "Nhà cung cấp",
        path: "/admin/category/supplier",
      },
    ],
  },
  {
    id: 6,
    name: "Quản lý nhập hàng",
    icon: <ImportExportIcon />,

    children: [
      {
        id: 7,
        name: "Danh sách nhập hàng",
        path: "/admin/import/list",
      },
      {
        id: 8,
        name: "Nhập hàng",
        path: "/admin/import",
      },
    ],
  },
  {
    id: 9,
    name: "Quản lý sản phẩm",
    icon: <LayersIcon />,
    children: [
      {
        id: 10,
        name: "Danh sách sản phẩm",
        path: "/admin/product",
      },
      {
        id: 11,
        name: "Chương trình khuyến mãi",
        path: "/admin/product/promotion",
      },
      {
        id: 12,
        name: "Danh sách kiểm kê",
        path: "/admin/product/inventory",
      },
    ],
  },
  {
    id: 13,
    name: "Quản lý bán hàng",
    icon: <ShoppingCartIcon />,
    children: [
      {
        id: 14,
        name: "Danh sách đơn hàng",
        path: "/admin/order",
      },
    ],
  },
  {
    id: 15,
    name: "Quản lý tài khoản",
    icon: <PeopleIcon />,
    children: [
      {
        id: 16,
        name: "Danh sách tài khoản",
        path: "/admin/account",
      },
      {
        id: 17,
        name: "Phân quyền",
        path: "/admin/account/role",
      },
    ],
  },
  {
    id: 18,
    name: "Báo cáo thống kê",
    icon: <BarChartIcon />,
    path: "/admin/report",
  },
];
