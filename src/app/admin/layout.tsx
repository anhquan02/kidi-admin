'use client';
import { Box, Container, CssBaseline, Toolbar } from "@mui/material";
import LeftSide from "./common/LeftSide";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { use } from "react";

const layout = (props: any) => {
  return (
    <>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Box sx={{ display: "flex", height: "100vh", width: "1" }}>
          <CssBaseline />
          <LeftSide />
          <Box component={"main"} sx={{ width: 1 }}>
            <Toolbar />
            <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
              {props.children}
            </Container>
          </Box>
        </Box>
      </LocalizationProvider>
    </>
  );
};

export default layout;
