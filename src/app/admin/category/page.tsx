"use client";
import {
  Box,
  Button,
  Grid,
  Table,
  TextareaAutosize,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { Fragment, useEffect, useState } from "react";

interface Data {
  id: number;
  name: string;
  description: string;
}

const columns = [
  { id: "STT", label: "STT", minWidth: 100 },
  { id: "name", label: "Tên danh mục", minWidth: 100 },
  { id: "description", label: "Mô tả", minWidth: 100 },
  { id: "action", label: "Hành động", minWidth: 100 },
];

const CategoryPage = () => {
  const [page, setPage] = useState<any>(0);
  const [rowsPerPage, setRowsPerPage] = useState<any>(10);
  const [data, setData] = useState<Data[] | any[]>([]);

  useEffect(() => {
    setData([{ id: 1, name: "Danh mục 1", description: "Mô tả danh mục 1" }]);
  }, []);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Grid xs={12} spacing={3}>
      {/* form input include name and description */}
      <Grid container item xs={12}>
        <Grid item xs={2} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Tên danh mục</Typography>
        </Grid>
        <Grid item xs={10}>
          <TextField
            label="Tên danh mục"
            variant="outlined"
            fullWidth
            margin="normal"
          />
        </Grid>
        <Grid item xs={2} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Mô tả</Typography>
        </Grid>
        <Grid item xs={10}>
          <TextField
            id="outlined-multiline-flexible"
            label="Mô tả"
            multiline
            fullWidth
            margin="normal"
            rows={4}
          />
        </Grid>
      </Grid>
      <Grid item xs={12} sx={{ mt: 2 }}>
        {/* button in right bottom */}
        <Box sx={{ display: "flex", justifyContent: "flex-end", width: 1 }}>
          <Button variant="contained" fullWidth>
            Lưu
          </Button>
        </Box>
      </Grid>
      {/* table */}
      <Grid item xs={12} className="my-4 p-2 border rounded">
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {/* rows */}
              {data.map((row, index) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id}>
                          {column.id === "STT" ? index + 1 : ""}
                          {column.id === "action" ? (
                            <Fragment>
                              <Button variant="contained" className="mr-2">
                                Sửa
                              </Button>
                              <Button variant="contained" className="ml-2">
                                Xóa
                              </Button>
                            </Fragment>
                          ) : (
                            value
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 100]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Grid>
    </Grid>
  );
};

export default CategoryPage;
