"use client";
import {
  Box,
  Button,
  Grid,
  Table,
  TextareaAutosize,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { DatePicker } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import { Fragment, useEffect, useState } from "react";

interface Data {
  id: number;
  img: string;
  name: string;
  supplier: string;
  price: string;
  quantity: string;
}

const columns = [
  { id: "STT", label: "STT", minWidth: 100 },
  { id: "img", label: "Sản phẩm", minWidth: 100 },
  { id: "name", label: "Tên sản phẩm", minWidth: 100 },
  { id: "supplier", label: "Nhà cung cấp", minWidth: 100 },
  { id: "price", label: "Giá", minWidth: 100 },
  { id: "quantity", label: "Số lượng", minWidth: 100 },
];

const ReportPage = () => {
  const [page, setPage] = useState<any>(0);
  const [rowsPerPage, setRowsPerPage] = useState<any>(10);
  const [data, setData] = useState<Data[] | any[]>([]);

  useEffect(() => {
    setData([
      {
        id: 1,
        img: "",
        name: "Váy nơ đỏ",
        supplier: "RHH",
        price: "100000",
        quantity: "10",
      },
      {
        id: 2,
        img: "",
        name: "Áo thun trắng",
        supplier: "RHH",
        price: "50000",
        quantity: "20",
      },
    ]);
  }, []);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Grid xs={12} spacing={3}>
      <Grid container item xs={12} spacing={2}>
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Từ ngày</Typography>
        </Grid>
        <Grid item xs={5}>
          <DatePicker
            value={dayjs(new Date())}
            onChange={(date) => console.log(date)}
            className="w-full"
          />
        </Grid>
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Đến ngày</Typography>
        </Grid>
        <Grid item xs={5}>
          <DatePicker
            value={dayjs(new Date())}
            onChange={(date) => console.log(date)}
            className="w-full"
          />
        </Grid>
        {/* input sản phẩm */}
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Sản phẩm</Typography>
        </Grid>
        <Grid item xs={5}>
          <TextField
            label="Sản phẩm"
            variant="outlined"
            fullWidth
            margin="normal"
          />
        </Grid>
      </Grid>
      <Grid item xs={4} sx={{ mt: 2 }}>
        {/* button in right bottom */}
        <Box sx={{ display: "flex", justifyContent: "flex-end", width: 1 }}>
          {/* btn tìm kiếm và hủy */}
          <Button variant="contained" color="info" className="mr-2">
            Tìm kiếm
          </Button>
          <Button variant="contained" color="warning" className="ml-2">
            Hủy
          </Button>
          {/* xuất báo cáo */}
          <Button variant="contained" color="success" className="ml-2">
            Xuất báo cáo
          </Button>
        </Box>
      </Grid>
      {/* table */}
      <Grid item xs={12} className="my-4 p-2 border rounded">
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {/* rows */}
              {data.map((row, index) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id}>
                          {column.id === "STT" ? index + 1 : ""}
                          {column.id === "action" ? (
                            <Fragment>
                              <Button variant="contained" className="mr-2">
                                Sửa
                              </Button>
                              <Button variant="contained" className="ml-2">
                                Xóa
                              </Button>
                            </Fragment>
                          ) : (
                            value
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 100]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Grid>
    </Grid>
  );
};

export default ReportPage;
