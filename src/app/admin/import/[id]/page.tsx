"use client";
import {
  Box,
  Button,
  Grid,
  Table,
  TextareaAutosize,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { Fragment, useEffect, useState } from "react";

interface Data {
  id: number;
  name: string;  
  price: string;
  supplier: string;
  quantity: number;
  quality: number;
}

const columns = [
  { id: "STT", label: "STT", minWidth: 100 },
  { id: "productId", label: "Mã sản phẩm", minWidth: 100 },
  { id: "name", label: "Sản phẩm", minWidth: 100 },
  { id: "supplier", label: "Nhà cung cấp", minWidth: 100 },
  { id: "price", label: "Giá nhập", minWidth: 100 },
  { id: "quantity", label: "Số lượng", minWidth: 100 },
  { id: "quality", label: "Phẩm chất", minWidth: 100 },
];

const ImportDetail = () => {
  const [page, setPage] = useState<any>(0);
  const [rowsPerPage, setRowsPerPage] = useState<any>(10);
  const [data, setData] = useState<Data[] | any[]>([]);

  useEffect(() => {
    setData([
      {
        id: 1,
        name: "Váy hoa mai",
        supplier: "NJC002",
        price: "100000",
        quantity: 10,
        quality: 1,
      },
      {
        id: 2,
        name: "Áo dài cách tân",
        supplier: "NJC003",
        price: "100000",
        quantity: 20,
        quality: 1,
      },
      {
        id: 3,
        name: "Áo dài cách tân",
        supplier: "NJC003",
        price: "100000",
        quantity: 20,
        quality: 1,
      },
      {
        id: 4,
        name: "Áo dài cách tân",
        supplier: "NJC003",
        price: "100000",
        quantity: 20,
        quality: 1,
      },
      {
        id: 5,
        name: "Áo dài cách tân",
        supplier: "NJC003",
        price: "100000",
        quantity: 20,
        quality: 1,
      },
      {
        id: 6,
        name: "Áo dài cách tân",
        supplier: "NJC003",
        price: "100000",
        quantity: 20,
        quality: 1,
      },
      {
        id: 7,
        name: "Áo dài cách tân",
        supplier: "NJC003",
        price: "100000",
        quantity: 20,
        quality: 1,
      },
      {
        id: 8,
        name: "Áo dài cách tân",
        supplier: "NJC003",
        price: "100000",
        quantity: 20,
        quality: 1,
      },
      {
        id: 9,
        name: "Áo dài cách tân",
        supplier: "NJC003",
        price: "100000",
        quantity: 20,
        quality: 1,
      },
      {
        id: 10,
        name: "Áo dài cách tân",
        supplier: "NJC003",
        price: "100000",
        quantity: 20,
        quality: 1,
      }
    ]);
  }, []);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Grid xs={12} spacing={3}>
      {/* form input include name and description */}
      <Grid container item xs={12} className="mb-4">
        {/* btn Thêm */}
        <Box sx={{ display: "flex", justifyContent: "flex-start", width: 1 }}>
          {/* Typhography  */}
          <Typography variant="h5">Chi tiết nhập hàng</Typography>
        </Box>
      </Grid>
      <Grid container item xs={12} spacing={2}>
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Người nhập</Typography>
        </Grid>
        <Grid item xs={5}>
          <TextField
            label="Người nhập"
            variant="outlined"
            fullWidth
            margin="normal"
          />
        </Grid>
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Ngày nhập</Typography>
        </Grid>
        <Grid item xs={5}>
          <TextField
            id="outlined-multiline-flexible"
            label="Ngày nhập"
            fullWidth
            margin="normal"
          />
        </Grid>
      </Grid>
      {/* table */}
      <Grid item xs={12} className="my-4 p-2 border rounded">
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {/* rows */}
              {data.map((row, index) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id}>
                          {column.id === "STT" ? index + 1 : ""}
                          {column.id === "quality" ? (
                            <Fragment>
                              {/* select Tốt hoặc Xấu */}
                              <FormControl component="fieldset">
                                <RadioGroup
                                  row
                                  aria-label="quality"
                                  name="row-radio-buttons-group"
                                >
                                  <FormControlLabel
                                    value="good"
                                    control={<Radio />}
                                    label="Tốt"
                                  />
                                  <FormControlLabel
                                    value="bad"
                                    control={<Radio />}
                                    label="Xấu"
                                  />
                                </RadioGroup>
                              </FormControl>
                            </Fragment>
                          ) : (
                            value
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 100]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Grid>
      <Grid xs={12} sx={{ display: "flex", justifyContent: "flex-end" }}>
        <Button variant="contained">
          Lưu
        </Button>
        </Grid>
    </Grid>
  );
};

export default ImportDetail;
