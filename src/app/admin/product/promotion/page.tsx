"use client";
import {
  Box,
  Button,
  Grid,
  Table,
  TextareaAutosize,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { DatePicker } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import { Fragment, useEffect, useState } from "react";
interface Data {
  id: number;
  code: string;
  discount: string;
  dayStart: string;
  dayEnd: string;
  quantity: string;
  condition: string;
}

const columns = [
  { id: "STT", label: "STT", minWidth: 100 },
  { id: "code", label: "Mã khuyến mãi", minWidth: 100 },
  { id: "discount", label: "Số tiền giảm", minWidth: 100 },
  { id: "dayStartEnd", label: "Ngày áp dụng", minWidth: 100 },
  { id: "quantity", label: "Số lượng", minWidth: 100 },
  { id: "condition", label: "Điều kiện", minWidth: 100 },
  { id: "action", label: "Hành động", minWidth: 100 },
];

const PromotionPage = () => {
  const [page, setPage] = useState<any>(0);
  const [rowsPerPage, setRowsPerPage] = useState<any>(10);
  const [data, setData] = useState<Data[] | any[]>([]);

  const [formData, setFormData] = useState<any>({
    code: "",
    discount: "",
    dayStart: "",
    dayEnd: "",
    quantity: "",
    condition: "",
  });
  useEffect(() => {
    setData([
      {
        id: 1,
        code: "MKB1",
        discount: "30000",
        dayStart: "06/04/2024",
        dayEnd: "10/04/2024",
        quantity: "100",
        condition: "100000",
      },
    ]);
  }, []);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Grid xs={12} spacing={3}>
      {/* form input include name and description */}
      <Grid container item xs={12} spacing={2}>
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Mã khuyến mãi</Typography>
        </Grid>
        <Grid item xs={5}>
          <TextField
            label="Mã khuyến mãi"
            variant="outlined"
            fullWidth
            margin="normal"
          />
        </Grid>
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Tiền giảm</Typography>
        </Grid>
        <Grid item xs={5}>
          <TextField
            id="outlined-multiline-flexible"
            label="Tiền giảm"
            fullWidth
            margin="normal"
          />
        </Grid>
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Từ ngày</Typography>
        </Grid>
        <Grid item xs={5}>
          <DatePicker
            value={dayjs(new Date())}
            onChange={(date) => console.log(date)}
            className="w-full"
          />
        </Grid>

        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Đến ngày</Typography>
        </Grid>
        <Grid item xs={5}>
          <DatePicker
            value={dayjs(new Date())}
            onChange={(date) => console.log(date)}
            className="w-full"
          />
        </Grid>
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Số lượng</Typography>
        </Grid>
        <Grid item xs={5}>
          <TextField
            label="Số lượng"
            variant="outlined"
            fullWidth
            margin="normal"
          />
        </Grid>
        <Grid item xs={1} sx={{ display: "flex", alignItems: "center" }}>
          <Typography>Giá đơn hàng</Typography>
        </Grid>
        <Grid item xs={5}>
          <TextField
            id="outlined-multiline-flexible"
            label="Giá đơn hàng"
            fullWidth
            margin="normal"
          />
        </Grid>
      </Grid>
      <Grid item xs={12} sx={{ mt: 2 }}>
        {/* button in right bottom */}
        <Box sx={{ display: "flex", justifyContent: "flex-end", width: 1 }}>
          <Button variant="contained" fullWidth>
            Lưu
          </Button>
        </Box>
      </Grid>
      {/* table */}
      <Grid item xs={12} className="my-4 p-2 border rounded">
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {/* rows */}
              {data.map((row, index) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id}>
                          {column.id === "STT" ? index + 1 : ""}
                          {column.id === "dayStartEnd"
                            ? `${row.dayStart} - ${row.dayEnd}`
                            : ""}
                          {column.id === "action" ? (
                            <Fragment>
                              <Button variant="contained" className="mr-2">
                                Sửa
                              </Button>
                              <Button variant="contained" className="ml-2">
                                Xóa
                              </Button>
                            </Fragment>
                          ) : (
                            value
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 100]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Grid>
    </Grid>
  );
};

export default PromotionPage;
